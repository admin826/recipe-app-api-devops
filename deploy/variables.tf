variable "prefix" {
  type        = string
  default     = "raad"
  description = "description"
}
variable "project" {
  type    = string
  default = "recipe-app-api-devops"
}

variable "contact" {
  type    = string
  default = "admin@eaastech.com"
}